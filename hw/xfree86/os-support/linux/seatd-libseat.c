/*
 * Copyright © 2022 Mark Hindley, Ralph Ronnquist.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Authors: Mark Hindley <mark@hindley.org.uk>
 *          Ralph Ronnquist <ralph.ronnquist@gmail.com>
 */

#ifdef HAVE_XORG_CONFIG_H
#include <xorg-config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <libseat.h>

#include "os.h"
#include "linux.h"
#include "xf86.h"
#include "xf86platformBus.h"
#include "xf86Xinput.h"
#include "xf86Priv.h"
#include "globals.h"
#include "seatd-libseat.h"

// ==========================================================
// ============ libseat client adapter ======================

struct libseat_info {
    char *session;
    Bool active;
    Bool vt_active;
    /*
     * This pointer gets initialised to the actual libseat client instance
     * provided by libseat_open_seat.
     */
    struct libseat *client;
    int graphics_id;
};
static struct libseat_info seat_info;

/*
 * The seat has been enabled, and is now valid for use. Re-open all
 * seat devices to ensure that they are operational, as existing fds
 * may have had their functionality blocked or revoked.
 */
static void enable_seat(struct libseat *seat,void *userdata) {
    (void) userdata;
    LogMessage( X_INFO, "seatd_libseat enable\n" );
    seat_info.active = TRUE;
    seat_info.vt_active = TRUE;
    xf86VTEnter();
}

/*
 * The seat has been disabled. This event signals that the application
 * is going to lose its seat access. The event *must* be acknowledged
 * with libseat_disable_seat shortly after receiving this event.
 *
 * If the recepient fails to acknowledge the event in time, seat
 * devices may be forcibly revoked by the seat provider.
 */
static void disable_seat(struct libseat *seat, void *userdata) {
    (void) userdata;
    LogMessage( X_INFO, "seatd_libseat disable\n" );
    seat_info.vt_active = FALSE;
    xf86VTLeave();
    if ( libseat_disable_seat( seat ) ) {
	LogMessage( X_ERROR, "seatd_libseat disable failed: %d\n", errno );
    }
}

/*
 * Callbacks for handling the libseat events.
 */
static struct libseat_seat_listener client_callbacks = {
    .enable_seat = enable_seat,
    .disable_seat = disable_seat,
};

/*
 * Check libseat is initialised and active.
 */
static Bool libseat_active(void) {
    if ( ! seat_info.client ) {
	LogMessage( X_ERROR, "seatd_libseat not initialised!\n" );
        return FALSE;
    }
    if ( ! seat_info.active ) {
	LogMessage( X_ERROR, "seatd_libseat not active\n" );
	return FALSE;
    }
    return TRUE;
}

/*
 * Handle libseat events
 */
static int libseat_handle_events(int timeout) {
    int ret;
    while ( (ret = libseat_dispatch( seat_info.client, timeout )) > 0)
        LogMessage( X_INFO, "seatd_libseat handled %i events\n", ret);
    if (ret == -1) {
        LogMessage(X_ERROR, "libseat_dispatch() failed: %s\n", strerror(errno));
        return -1;
    }
    return ret;
}

/*
 * Handle libseat logging.
 */
static void log_libseat(enum libseat_log_level level,
                        const char *fmt,
                        va_list args) {
    MessageType xmt;
    char xfmt[strlen(fmt)+2];
    snprintf(xfmt, sizeof(xfmt), "%s\n", fmt);
    switch (level) {
    case LIBSEAT_LOG_LEVEL_INFO:
        xmt = X_INFO;
        break;
    case LIBSEAT_LOG_LEVEL_ERROR:
        xmt = X_ERROR;
        break;
    default:
         xmt = X_DEBUG;
    }
    LogVMessageVerb(xmt, 0, xfmt, args);
}

// ==========================================================
// ============== seatd-libseat.h API functions =============

/*
 * Initialise the libseat client.
 *
 * Returns:
 *   0    if all ok
 *   -EPERM (-1) if it was already initialised
 *   -EPIPE (-32) if the seat opening failed.
 */
int seatd_libseat_init(void) {
    libseat_set_log_level(LIBSEAT_LOG_LEVEL_DEBUG);
    libseat_set_log_handler(log_libseat);
    LogMessage( X_INFO, "seatd_libseat init\n" );
    if (libseat_active()) {
	LogMessage( X_ERROR, "seatd_libseat already initialised\n" );
	return -EPERM;
    }
    seat_info.graphics_id = -1;
    seat_info.client = libseat_open_seat( &client_callbacks, NULL );
    if ( ! seat_info.client ) {
	LogMessage( X_ERROR, "Cannot set up seatd_libseat client\n" );
	return -EPIPE;
    }
    if (libseat_handle_events(100) < 0) {
        libseat_close_seat(seat_info.client);
        return -EPIPE;
    }
    LogMessage( X_INFO, "seatd_libseat client activated\n" );
    return 0;
}

/*
 * Shutdown the libseat client.
 */
void seatd_libseat_fini(void) {
    LogMessage( X_INFO, "seatd_libseat fini\n" );
    if ( seat_info.client )
        libseat_close_seat( seat_info.client );
    seat_info.graphics_id = -1;
    seat_info.active = FALSE;
    seat_info.client = NULL;
}

/*
 * Open the graphics device
 *
 * Return
 *   file descriptor (>=0) if all is ok.
 *   -EPERM (-1) if the libseat client is not activated
 *   -EAGAIN (-11) if the VT is not active
 *   -errno from libseat_open_device if device access failed
 */
int seatd_libseat_open_graphics(const char *path) {
    int fd, id;
    LogMessage( X_INFO, "seatd_libseat try open graphics %s\n", path );
    if ( ! libseat_active() ) {
	return -EPERM;
    }
    if ( (id = libseat_open_device( seat_info.client, path, &fd )) == -1 ) {
	fd = -errno;
        LogMessage( X_ERROR, "seatd_libseat open graphics %s (%d) failed: %d\n", path, id, fd );
    } else {
        LogMessage( X_INFO, "seatd_libseat opened graphics: %s (%d:%d)\n", path, id, fd );
    }
    seat_info.graphics_id = id;
    return fd;
}

/*
 * Open an input device.
 *
 * The function sets the p->options "libseat_id" for the device when
 * successful.
 */
void seatd_libseat_open_device(InputInfoPtr p) {
    int id, fd;
    char *path = xf86CheckStrOption(p->options, "Device", NULL);
    LogMessage( X_INFO, "seatd_libseat try open %s\n", path );
    if ( ! libseat_active() ) {
	return;
    }
    if ( (id = libseat_open_device( seat_info.client, path, &fd) ) == -1 ) {
	fd = -errno;
        LogMessage( X_ERROR, "seatd_libseat open %s (%d) failed: %d\n", path, id,  fd );
    } else {
        p->options = xf86ReplaceIntOption( p->options, "fd", fd );
        p->options = xf86ReplaceIntOption( p->options, "libseat_id", id );
        LogMessage( X_INFO, "seatd_libseat opened %s (%d:%d)\n", path, id,  fd );
    }
}

/*
 * Release an input device.
 */
void seatd_libseat_close_device(InputInfoPtr p) {
    char *path = xf86CheckStrOption(p->options, "Device", NULL);
    int fd = xf86CheckIntOption(p->options, "fd", -1);
    int id = xf86CheckIntOption(p->options, "libseat_id", -1);
    LogMessage( X_INFO, "seatd_libseat try close %s (%d:%d)\n", path, id, fd );
    if (!libseat_active())
        return;
    if ( fd < 0 ) {
        LogMessage( X_ERROR, "seatd_libseat device not open (%s)\n", path );
	return;
    }
    if ( id < 0 ) {
        LogMessage( X_ERROR, "seatd_libseat no libseat ID\n" );
	return;
    }
    if ( libseat_close_device( seat_info.client, id ) ) {
	LogMessage( X_ERROR, "seatd_libseat close failed %d\n", -errno );
    }
    close( fd );
}
